#include <sqlite3.h>
#include <iostream>
#include <string>

static int callback(void *NotUsed, int argc, char **argv, char **azColName);
void checkIfOperationDone(int rc, std::string message, char* errorMessage);

int main() {

  sqlite3* db;
  int rc = sqlite3_open("bicom.db", &db);

  if(rc) std::cout << "Can't open database, reason: " << sqlite3_errmsg(db) << std::endl;
  else std::cout << "Database opened successfully!" << std::endl;


  char* sql = "CREATE TABLE EMPLOYEES(" \
              "ID    INT PRIMARY KEY NOT NULL," \
              "NAME  TEXT            NOT NULL," \
              "AGE   INT             NOT NULL)";

  char* errorMessage = 0;
  rc = sqlite3_exec(db, sql, callback, 0, &errorMessage);
  checkIfOperationDone(rc, "Table created successfully", errorMessage);

  sql = "INSERT INTO EMPLOYEES (ID, NAME, AGE) " \
        "VALUES (1, 'Elvis', 30); " \
        "INSERT INTO EMPLOYEES (ID, NAME, AGE) " \
        "VALUES (2, 'Kasim', 22);";

  //errorMessage = 0; 
  //rc = sqlite3_exec(db, sql, callback, 0, &errorMessage);
  //checkIfOperationDone(rc, "Data entered successfully", errorMessage);


  std::string sqlStmt = "SELECT * FROM EMPLOYEES WHERE NAME = ?";
  sqlite3_stmt* stmt;
  sqlite3_prepare_v2(db, sqlStmt.c_str(), sqlStmt.length(), &stmt, nullptr);

  std::string name = "Elvis";
  sqlite3_bind_text(stmt, 1, name.c_str(), name.length(), SQLITE_STATIC);

  while(sqlite3_step(stmt) == SQLITE_ROW) {
    int col_id = sqlite3_column_int(stmt, 0);
    const unsigned char* col_name = sqlite3_column_text(stmt, 1);
    int col_age = sqlite3_column_int(stmt, 2);
    
    std::cout << "ID: " << col_id << std::endl;
    std::cout << "NAME: " << col_name << std::endl;
    std::cout << "AGE: " << col_age << std::endl << std::endl; 
  }

  sqlite3_finalize(stmt);

  // old
  
  //errorMessage = 0;
  //char* data = "Callback function called";

  //rc = sqlite3_exec(db, sql, callback, (void*)data, &errorMessage);
  //checkIfOperationDone(rc, "Data selected successfully", errorMessage);

  //old -- end

  sqlite3_close(db);
  return 0;
}

void checkIfOperationDone(int rc, std::string message, char* errorMessage) {
  if(rc != SQLITE_OK) {
    std::cout << "SQL error: " << errorMessage << std::endl;
    sqlite3_free(errorMessage);
  }
  else std::cout << message << std::endl;
}

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
   int i;
   for(i = 0; i < argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}
